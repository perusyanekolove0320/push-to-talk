#! /bin/bash -ex
PROJECT_NAME='event-network-push-to-talk'
TAG='2'

IMAGE_NAME="${PROJECT_NAME}:${TAG}"
REPOSITORY_NAME="derushio.work:5000/${IMAGE_NAME}"

sudo docker build --build-arg NODE_ENV=production -t "${IMAGE_NAME}" .
sudo docker tag "${IMAGE_NAME}" "${REPOSITORY_NAME}"

sudo docker push "${REPOSITORY_NAME}"
echo $REPOSITORY_NAME;
