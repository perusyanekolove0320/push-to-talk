import express from 'express';
import bodyParser from 'body-parser';
import * as path from 'path';

const app = express();
const db = [] as any[];
//TODO: 元々id毎の部屋を用意したい

const port = 8080;
const execPath = path.dirname(process.argv[1]);
const publicPath = path.resolve(execPath, '../public');

export async function initServer(): Promise<void> {
    app.use(express.static(publicPath));
    app.use(
        bodyParser.urlencoded({
            extended: true,
        }),
    );

    app.post('/speakStart', () => {
        const startTime = {
            startTime: Date.now(),
        };
        db.push(startTime);
    });

    app.post('/speakStop', () => {
        const endTime = {
            endTime: Date.now(),
        };
        db.push(endTime);
    });

    function calculateSpeakingTime(db: any[]): number {
        let n = 1;
        let speakingTime = 0;
        while (n < db.length) {
            speakingTime += db[n].endTime - db[n - 1].startTime;
            n += 2;
        }
        return speakingTime / 1000;
    }

    app.post('/result', (req, res) => {
        if (db.length === 0) {
            //エラー処理（配列の要素が０の時エラー吐く）
            console.log('error');
            res.status(200).json({
                error: 'データがありません。会話を始めてください。'
            });
            db.length = 0;//配列の要素を削除し、リセット
        } else {
            //話していた時間の計算

            //合計時間の取得(totalTime)
            const totalTime =
                (db[db.length - 1].endTime - db[0].startTime) / 1000;
            //話していた時間の取得(speakingTime)
            const speakingTime = calculateSpeakingTime(db);
            //話していた時間の割合の取得(ratio)
            const ratio = Math.round((speakingTime / totalTime) * 100);

            res.status(200).json({
                //レスポンスコード =>特定のHTTP通信が成功したか否か （例）今は200番で、リクエストが成功なら、その本文を返す。
                totalTime: totalTime + '秒',
                speakingTime: speakingTime + '秒',
                ratioForSpeaking: ratio + '%',
            });
            console.log(db); //実データの確認
            db.length = 0; //配列の要素を削除しリセット
        }
    });

    app.listen(port, () => console.log(`Listening on port ${port}...`));
}
